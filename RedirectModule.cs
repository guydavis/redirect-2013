﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Linq;
using Microsoft.SharePoint.Utilities;
using System.Threading;
using System.Web.Configuration;

namespace Redirect2013
{
    public class SpRedirect : IHttpModule
    {

        const int activeColumn = 0;
        const int urlColumn = 1;
        const int targetColumn = 2;

        private bool isInitialized = false;
        private bool bypassSharePoint = true;

        private Dictionary<string, RedirectRecord> _sites = new Dictionary<string, RedirectRecord>();

        private string migrationDirectory = string.Empty;

        private string migrationFileLocation = string.Empty;
        private string updatesFileLocation = string.Empty;
        private string logging = string.Empty;

        private string logFileLocation = string.Empty;
        private string template = string.Empty;
        private string redirectHTMLTemplate = string.Empty;
        private string futureRedirectHTMLTemplate = string.Empty;
        private string fileRedirectHTMLTemplate = string.Empty;
        private string cookieLength = "1";
        private string[] fileTypes;
        private string[] testIPAddrs;

        public void Dispose()
        {
        }

        private void UpdateSitesDictionary()
        {
            LogMessage("check for zero sites");
            if (_sites.Count == 0)
            {
                LogMessage("zero sites");
                UpdateSitesDirectoryForPath(migrationFileLocation);
            }
            //LogMessage(string.Format("check for updates file at: {0}", updatesFileLocation));
            //if (File.Exists(updatesFileLocation))
            //{
            //    LogMessage("update file exists");
            //    UpdateSitesDirectoryForPath(updatesFileLocation);
            //    File.Delete(updatesFileLocation);
            //}
        }

        private void UpdateSitesDirectoryForPath(string fileLocation)
        {
            TextReader reader = File.OpenText(fileLocation);
            string header = reader.ReadLine();
            while (true)
            {
                string record = reader.ReadLine();
                if (record == null)
                {
                    reader.Close();
                    reader.Dispose();
                    break;
                }

                string[] rec = record.Split('\t');
                string key = rec[urlColumn].ToLower().Trim();

                if (!_sites.ContainsKey(key))
                {
                    if (key.Length > 0 && rec[targetColumn].Length > 0)
                    {
                        CreateRedirectRecord(key, rec);
                    }
                }
                else
                {
                    UpdateRedirectRecord(key, rec);
                }
            }
        }

        private void SetCookie(HttpApplication context, string url)
        {
            LogMessage(string.Format("{2}: Setting {0} minute cookie for {1}", cookieLength, url, context.Request.UserHostAddress));
            context.Response.Cookies[url].Value = "Y";
            context.Response.Cookies[url].Expires = DateTime.Now.AddMinutes(Convert.ToInt32(cookieLength));
        }

        private void FileMatchFoundRedirect(HttpApplication context, IncomingURI incomingUri, RedirectRecord recFound)
        {
            //RedirectRecord recFound = _sites[incomingUri.AbsoluteURIValue];

            string uriReplaceSpaces = SPEncode.UrlDecodeAsUrl(incomingUri.AbsoluteIncomingURIValue);
            string outgoing = uriReplaceSpaces.Replace(recFound.URL, recFound.Target);

            //            LogMessage(string.Format("{1}: FileMatchFoundRedirect - {0}", outgoing, context.Request.UserHostAddress));
            if (context.Request.Cookies.Get(recFound.URL) == null)
            {
                LogMessage(string.Format("{0}: FileMatchFoundRedirect - Setting Cookie", context.Request.UserHostAddress));
                SetCookie(context, recFound.URL);

                string message = fileRedirectHTMLTemplate;
                string response = template.Replace("{TARGET}", outgoing).Replace("{SOURCE}", recFound.URL);
                message = message.Replace("{TARGET}", outgoing).Replace("{SOURCE}", recFound.URL);
                response = response.Replace("{REDIRECT_MESSAGE}", message);

                LogMessage(string.Format("{1}: FileMatchFoundRedirect - Setting Cookie and Redirecting: {0}", outgoing, context.Request.UserHostAddress));
                WriteResponse(context, response);
            }
            else
            {
                LogMessage(string.Format("{1}: FileMatchFoundRedirect - Cookie was already set just Redirecting: {0}", outgoing, context.Request.UserHostAddress));
                context.Response.Redirect(outgoing, false);
                return;
            }
        }

        private void MatchFoundRedirect(HttpApplication context, IncomingURI incomingUri, RedirectRecord recFound)
        {
            // new logic here must handle aspx, files and default.aspx

            //RedirectRecord recFound = _sites[incomingUri.AbsoluteURIValue];
            string outgoing = incomingUri.AbsoluteIncomingURIValue;
            string message = futureRedirectHTMLTemplate;
            if (recFound.Active)
            {
                string uriReplaceSpaces = SPEncode.UrlDecodeAsUrl(incomingUri.AbsoluteIncomingURIValue);
                outgoing = uriReplaceSpaces.Replace(recFound.URL, recFound.Target);
                LogMessage(string.Format("{2}: Redirect is active, URL: {0}, Target: {1}", recFound.URL, recFound.Target, context.Request.UserHostAddress));
                message = redirectHTMLTemplate;
                LogMessage(string.Format("{1}: Redirect is active, Redirecting to: {0}", outgoing, context.Request.UserHostAddress));
            }

            string target = string.Format("{0}{1}redirectProcessed=true", outgoing, outgoing.Contains("?") ? "&" : "?");
            if (target.Contains("default.aspx"))
            {
                // yank default.aspx
                target = target.Replace("default.aspx", string.Empty);
            }
            if (context.Request.Cookies.Get(recFound.URL) == null)
            {
                LogMessage(string.Format("{1}: Setting Cookie and Redirecting: {0}", outgoing, context.Request.UserHostAddress));
                SetCookie(context, recFound.URL);

                string response = template.Replace("{TARGET}", target).Replace("{SOURCE}", incomingUri.IncomingURIValue);
                message = message.Replace("{TARGET}", target).Replace("{SOURCE}", incomingUri.IncomingURIValue);
                response = response.Replace("{REDIRECT_MESSAGE}", message);

                WriteResponse(context, response);
            }
            else
            {
                LogMessage(string.Format("{1}: Cookie was already set just Redirecting: {0}", target, context.Request.UserHostAddress));
                if (recFound.Active)
                {
                    context.Response.Redirect(target, false);
                    return;
                }
                else
                {
                    //                    LogMessage(string.Format("{1}: Site not active bypassing response redirect for folders: {0}", target, context.Request.UserHostAddress));
                }
            }
        }

        private void WriteResponse(HttpApplication context, string response)
        {
            context.Response.Clear();
            context.Response.Write(response);
            context.Response.Flush();
            context.Response.End();
        }

        private void CreateRedirectRecord(string key, string[] record)
        {
            LogMessage(string.Format("Create key: {0}, target: {1}, active: {2}", key, record[targetColumn], record[activeColumn]));
            _sites.Add(key,
                new RedirectRecord(
                    key,
                    record[targetColumn].Trim(),
                    record[activeColumn].ToLower() == "y" ? true : false
                ));
        }

        private void UpdateRedirectRecord(string key, string[] record)
        {
            LogMessage(string.Format("Update key: {0}, target: {1}, active: {2}", key, record[targetColumn], record[activeColumn]));
            RedirectRecord redirectRec = _sites[key];
            redirectRec.Target = record[targetColumn].Trim();
            redirectRec.Active = record[activeColumn].ToLower() == "y" ? true : false;
            _sites[key] = redirectRec;
        }

        private void ReadWebConfigSettings()
        {
            logging = WebConfigurationManager.AppSettings["LowesRedirector:Logging"];
            migrationDirectory = WebConfigurationManager.AppSettings["LowesRedirector:MigrationDirectory"];    // must end with /
            
            string checkSharePoint = WebConfigurationManager.AppSettings["LowesRedirector:CheckSharePointUrls"];
            if (checkSharePoint.Length > 0)
            {
                bypassSharePoint = false;
            }

            migrationFileLocation = string.Format("{0}{1}", migrationDirectory, WebConfigurationManager.AppSettings["LowesRedirector:MigrationFile"]);
            updatesFileLocation = string.Format("{0}Updates.txt", migrationDirectory);
            string templateLocation = string.Format("{0}{1}", migrationDirectory, WebConfigurationManager.AppSettings["LowesRedirector:PageTemplate"]);
            template = File.ReadAllText(templateLocation);

            string redirectTemplateLocation = string.Format("{0}{1}", migrationDirectory, WebConfigurationManager.AppSettings["LowesRedirector:MessageTemplate"]);
            redirectHTMLTemplate = File.ReadAllText(redirectTemplateLocation);

            string futureRedirectTemplateLocation = string.Format("{0}{1}", migrationDirectory, WebConfigurationManager.AppSettings["LowesRedirector:FutureMessageTemplate"]);
            futureRedirectHTMLTemplate = File.ReadAllText(futureRedirectTemplateLocation);

            string fileRedirectTemplateLocation = string.Format("{0}{1}", migrationDirectory, WebConfigurationManager.AppSettings["LowesRedirector:FileTemplate"]);
            fileRedirectHTMLTemplate = File.ReadAllText(fileRedirectTemplateLocation);

            cookieLength = string.IsNullOrEmpty(WebConfigurationManager.AppSettings["LowesRedirector:CookieDuration"]) ?
                cookieLength : WebConfigurationManager.AppSettings["LowesRedirector:CookieDuration"];

            logFileLocation = string.Format(@"{0}lowesmigration_{1}.log", migrationDirectory, DateTime.Now.ToString("yyyyMMdd"));

            fileTypes = WebConfigurationManager.AppSettings["LowesRedirector:FileTypes"].ToString().Split(',');
            testIPAddrs = WebConfigurationManager.AppSettings["LowesRedirector:TestIPs"].ToString().Split(',');

            isInitialized = true;
        }

        public void LogMessage(string message)
        {
            if (logging.ToUpper() == "Y" || logging.ToUpper() == "YES") { 
                System.IO.StreamWriter file = new System.IO.StreamWriter(logFileLocation, true);
                file.WriteLine(message);

                file.Close();
            }
        }

        public string GetMatchedKey(string sourceUri)
        {
            bool isFullyParsed = false;
            bool matchFound = false;

            while (!isFullyParsed && !matchFound)
            {
                int lastSlash = sourceUri.LastIndexOf("/");
                if ((sourceUri.Length - 1) != lastSlash)    // we're at the end of the uri
                {
                    sourceUri = sourceUri.Substring(0, lastSlash+1);
                }

                if (sourceUri.EndsWith("//"))
                {
                    LogMessage("Source Uri ends with double slash");
                    isFullyParsed = true;
                    sourceUri = string.Empty;
                }
                else
                {
                    LogMessage(string.Format("Does _sites contain key '{0}'", sourceUri));
                    if (_sites.ContainsKey(sourceUri))
                    {
                        matchFound = true;
                    }
                    else
                    {
                        sourceUri = sourceUri.Substring(0, sourceUri.Length - 1);
                    }
                }
            }

            return sourceUri;
        }

        public void Init(HttpApplication context)
        {
            try
            {
                context.BeginRequest += (sender, args) =>
                {
                    if (!isInitialized)
                    {
                       ReadWebConfigSettings();
                    }

                    if (!AlreadyProcessed(context))
                    {
                        if ((NotTesting() || IsTestIP(context.Request.UserHostAddress)))
                        {   // for all requests / referrers
                            LogMessage("Not Testing or Testing and IP is correct");

                            string referrer = string.Empty;
                            if (context.Request.UrlReferrer != null)
                            {
                                referrer = context.Request.UrlReferrer.ToString();
                            }

                            string incoming = context.Request.Url.ToString().ToLower().Trim();
                            LogMessage(string.Format("{3}: Path: [{0}], Referrer: [{1}], Incoming: [{2}]", context.Request.RawUrl, referrer, incoming, context.Request.UserHostAddress));

                            IncomingURI incomingUri = new IncomingURI(incoming);

                            // only continue if this is SharePoint
                            if (bypassSharePoint || incomingUri.IsSharePoint)
                            {
                                LogMessage(string.Format("{1}: Path: <{0}> -- It's a SharePoint Site!", context.Request.RawUrl, context.Request.UserHostAddress));
                                UpdateSitesDictionary();
                                string foundKey = GetMatchedKey(bypassSharePoint ? incomingUri.AbsoluteIncomingURIValue : incomingUri.AbsoluteURIValue);
                                if (foundKey.Length > 0)
                                {
                                    RedirectRecord recFound = _sites[foundKey];
                                    LogMessage(string.Format("{1}: Path: <{0}> -- It's in the text file!", context.Request.RawUrl, context.Request.UserHostAddress));
                                    if (string.IsNullOrEmpty(incomingUri.FileName) || incomingUri.IsASPX)
                                    {
                                        LogMessage(string.Format("{1}: Path: <{0}> -- It's either aspx or just the site url!", context.Request.RawUrl, context.Request.UserHostAddress));
                                        MatchFoundRedirect(context, incomingUri, recFound);
                                    }
                                    else
                                    {
                                        if (recFound.Active)
                                        {
                                            LogMessage(string.Format("{1}: And this is the file we are checking: {0}", incomingUri.FileName, context.Request.UserHostAddress));
                                            foreach (string ext in fileTypes)
                                            {
                                                string extension = "." + ext;
                                                if (incomingUri.FileName.EndsWith(extension))
                                                {
                                                    FileMatchFoundRedirect(context, incomingUri, recFound);
                                                }
                                            }
                                        }
                                    }
                                }
                                // in SharePoint but not in the list of sites ignore
                            }
                        }
                        else if (!NotTesting() && IsTestIP(context.Request.UserHostAddress))
                        {
                           LogMessage(string.Format("{2}: Path: <{0}>, Incoming: <{1}>", context.Request.RawUrl, context.Request.Url.ToString().ToLower().Trim(), context.Request.UserHostAddress));
                        }
                    }
                };
            }
            catch (ThreadAbortException)
            {
                LogMessage(string.Format("***** Thread Exception ***** {0}", context.Request.UserHostAddress));
            }
            catch (Exception ex)
            {
                LogMessage(string.Format("***** Outer Exception ***** {1}: {0}", ex.Message, context.Request.UserHostAddress));
            }
        }

        private bool AlreadyProcessed(HttpApplication context)
        {
            return context.Request.QueryString["redirectProcessed"] != null;
        }

        private bool IsTestIP(string ipAddr)
        {
            return Array.IndexOf(testIPAddrs, ipAddr) >= 0;
        }

        private bool NotTesting()
        {
            return testIPAddrs[0].Length == 0;
        }
    }
}