﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SharePoint;

namespace Redirect2013
{
    public class IncomingURI {
        public bool IsSharePoint { get; set; }
        public string AbsoluteURIValue { get; set; }
        public string AbsoluteIncomingURIValue { get; set; }
        public string IncomingURIValue { get; set; }
        public bool IsASPX { get; set; }
        public string FileName { get; set; }
        public Uri URI { get; set; }

        public IncomingURI(string incoming)
        {
            try
            {
                IsSharePoint = false;
                AbsoluteURIValue = string.Empty;
                FileName = string.Empty;

                if (incoming.EndsWith("/"))
                {
                    incoming = incoming.Substring(0, incoming.Length - 1);
                }

                URI = new Uri(incoming);
                AbsoluteIncomingURIValue = URI.AbsoluteUri;
                FileName = URI.Query.Length > 0 ? URI.LocalPath.Replace(URI.Query, string.Empty) : URI.LocalPath;

                if (PageIsASPX())
                {
                    IsASPX = true;
                }

                IncomingURIValue = incoming;
                using (SPSite site = new SPSite(incoming))
                {
                    LogMessage("It's a site");
                    using (SPWeb web = site.OpenWeb())
                    {
                        LogMessage("It's a web");
                        IsSharePoint = true;
                        string webUrl = web.Url;
                        AbsoluteURIValue = webUrl.EndsWith("/") ? webUrl.Substring(0, webUrl.Length - 1) : webUrl;
                    }
                }
            }
            catch (Exception ex)    // is not a SharePoint url
            {
            }
        }

        public void LogMessage(string message)
        {
            //SharePointLogger.LogStartOfMethod();
            //SharePointLogger.LogMessageWithSeverityAndCategory(message, SharePointLogger.TraceSeverity.High, "Lowe's Redirect");
            //SharePointLogger.LogEndOfMethod();
        }

        private bool PageIsDefaultASPX()
        {
            string urlWithoutQuery = URI.Query.Length > 0 ? URI.LocalPath.Replace(URI.Query, string.Empty) : URI.LocalPath;
            urlWithoutQuery = urlWithoutQuery.ToLower();
            return urlWithoutQuery.EndsWith("default.aspx");
        }

        private bool PageIsASPX()
        {
            string urlWithoutQuery = URI.Query.Length > 0 ? URI.LocalPath.Replace(URI.Query, string.Empty) : URI.LocalPath;
            urlWithoutQuery = urlWithoutQuery.ToLower();
            return urlWithoutQuery.EndsWith(".aspx") || urlWithoutQuery.EndsWith(".htm") || urlWithoutQuery.EndsWith(".html");
        }

        private string GetAbsoluteWithoutQueryString()
        {
            string urlWithoutQuery = URI.Query.Length > 0 ? URI.LocalPath.Replace(URI.Query, string.Empty) : URI.LocalPath;
            urlWithoutQuery = urlWithoutQuery.ToLower();
            return urlWithoutQuery;
        }
    }
}