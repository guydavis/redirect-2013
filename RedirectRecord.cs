﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Redirect2013
{
    public class RedirectRecord
    {
        public string URL { get; set; }
        public string Target { get; set; }
        public bool Active { get; set; }

        public RedirectRecord(string url, string target, bool active)
        {
            URL = url;
            Target = target;
            Active = active;
        }
    }
}